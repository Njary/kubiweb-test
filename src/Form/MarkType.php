<?php

namespace App\Form;

use App\Entity\Mark;
use App\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('supplier', EntityType::class, array(
                'class' => Supplier::class,
                'choice_label' => 'title',
                'choice_value' => 'id',
                'attr'=>[
                      'required'=>'required'
                    ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mark::class,
        ]);
    }
}
