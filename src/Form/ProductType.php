<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Mark;
use App\Entity\Type;
use App\Entity\Kind;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('stockQty')
            ->add('priceTTC')
            ->add('mark',EntityType::class, array(
                'class' => Mark::class,
                'choice_label' => 'title',
                'choice_value' => 'id',
                'attr'=>[
                  'required'=>'required'
                ],))
            ->add('type',EntityType::class, array(
                'class' => Type::class,
                'choice_label' => 'title',
                'choice_value' => 'id',
                'attr'=>[
                      'required'=>'required'
                    ]))
            ->add('kind',EntityType::class, array(
                'class' => Kind::class,
                'choice_label' => 'title',
                'choice_value' => 'id',
                'attr'=>[
                      'required'=>'required'
                    ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
