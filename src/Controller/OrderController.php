<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use App\Repository\ClientRepository;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\CLient;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderController extends AbstractController
{	

    /**
     * @Route("/order", name="order")
     */
    public function index(Request $request, ClientRepository $clientRepository, SessionInterface $session)
    {
    	
    	$form = $this->createFormBuilder([])
    				->add('client', ChoiceType::class, [
                                'attr'=>[
                          'required'=>'required'
                        ],
    					'choices' => $clientRepository->findAll(), 
    					'choice_value' => 'id', 
    					'choice_label' => function(?Client $client) {
    						return $client ? $client->getFirstName().' '.$client->getName() : '';
    					}
    				])
    				->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	$data = $form->getData();

        	if ($session->has('clientId') && $session->get('clientId') !== $data['client']->getId()) {
        		$session->remove('orderId');
        	}

            if ($data['client'] != NULL)            
        	   return $this->redirectToRoute('product_list',['clientId'=>$data['client']->getId()]);
            else
                $this->addFlash('error', 'Client required!');
        }

        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/product-list/{clientId}", name="product_list")
     */
    public function productList(ProductRepository $productRepository, SessionInterface $session, OrderRepository $orderRepository, ClientRepository $clientRepository, int $clientId)
    {	
    	$products = $productRepository->findAll();

    	$order = NULL;

    	$client = $clientRepository->find($clientId);

    	if (!$session->has('orderId')) {
    		$order = new Order();
    		$order->setClient($client);
    		$order->setDate(new \DateTime('now'));
    		$order->setNumber(time());
            $order->setIsConfirmed(false);
    		
    		$entityManager = $this->getDoctrine()->getManager();

    		$entityManager->persist($order);
    		$entityManager->flush();

    		$session->set('orderId', $order->getId());
    		$session->set('clientId', $client->getId());
    	} else {
    		$order = $orderRepository->find($session->get('orderId'));
    	}

        return $this->render('order/product_list.html.twig', [
            'controller_name' => 'OrderController',
            'products' => $products,
            'client' => $client,
            'order' => $order
        ]);
    }

    /**
     * @Route("/order/add-product/{orderId}/{clientId}/{productId}", name="order_add_product")
     */
    public function addProduct(int $orderId, int $clientId, int $productId,ProductRepository $productRepository, OrderRepository $orderRepository, OrderItemRepository $orderItemRepository, ClientRepository $clientRepository)
    {	
    	$products = $productRepository->findAll();

    	$order = $orderRepository->find($orderId);

    	$product = $productRepository->find($productId);

        $product->retrieveQty(1);

    	$orderItem = $orderItemRepository->findOneBy(['product'=>$product,'parent'=>$order]);

    	$client = $clientRepository->find($clientId);

    	if ($orderItem == NULL) {
    		$orderItem= new OrderItem();
    		$orderItem->setParent($order);
    		$orderItem->setProduct($product);
    		$orderItem->setQty(1);
    	} else {
    		$orderItem->addQty(1);
    	}

    	$entityManager = $this->getDoctrine()->getManager();

		$entityManager->persist($orderItem);
        $entityManager->persist($product);
		$entityManager->flush();

        return $this->render('order/product_list.html.twig', [
            'controller_name' => 'OrderController',
            'products' => $products,
            'client' => $client,
            'order' => $order
        ]);
    }

    /**
     * @Route("/order/detail/{orderId}/{editable}", name="order_detail")
     */
    public function orderDetail(int $orderId,bool $editable = false, ProductRepository $productRepository, OrderRepository $orderRepository, OrderItemRepository $orderItemRepository, ClientRepository $clientRepository)
    {

    	$order = $orderRepository->find($orderId);


    	return $this->render('order/detail.html.twig', [
            'controller_name' => 'OrderController',
            'order' => $order,
            'isEditable' => isset($editable) ? $editable : false
        ]);
    }

    /**
     * @Route("/order/delete-item/{orderItemId}", name="delete_order_item")
     */
    public function deleteOrderItem(int $orderItemId, OrderItemRepository $orderItemRepository)
    {

    	$orderItem = $orderItemRepository->find($orderItemId);

    	$em = $this->getDoctrine()->getManager();

    	$em->remove($orderItem);

    	$em->flush();

    	return $this->render('order/detail.html.twig', [
            'controller_name' => 'OrderController',
            'order' => $orderItem->getParent(),
            'isEditable' => true
        ]);
    }

    /**
     * @Route("/order/confirm/{orderId}", name="confirm_order")
     */
    public function confirmOrder(int $orderId, ProductRepository $productRepository, OrderRepository $orderRepository, OrderItemRepository $orderItemRepository, ClientRepository $clientRepository, SessionInterface $session)
    {

    	$order = $orderRepository->find($orderId);

        $order->setIsConfirmed(true);

        $em = $this->getDoctrine()->getManager();

        $em->persist($order);
        
        $em->flush();

        $this->addFlash('success', 'Order saved successfuly!');

        $session->clear();

    	return $this->redirectToRoute('order_management',['clientId'=> $order->getClient()->getId()]);
    }

    /**
     * @Route("/order/edit/{orderItemId}", name="order_edit")
     */
    public function editOrderItem(int $orderItemId, Request $request, OrderItemRepository $orderItemRepository, SessionInterface $session)
    {
        
        $orderItem = $orderItemRepository->find($orderItemId);

        $form = $this->createFormBuilder(['qty'=> $orderItem->getQty()])
                    ->add('qty', TextType::class)
                    ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($data['qty'] > 0) {

                $oldQty = $orderItem->getQty();    

                $orderItem->setQty($data['qty']);

                $product = $orderItem->getProduct();

                $product->addQty($oldQty - $orderItem->getQty());

                $em = $this->getDoctrine()->getManager();

                $em->persist($orderItem);

                $em->persist($product);
                
                $em->flush();

                return $this->redirectToRoute('order_detail',['orderId'=> $orderItem->getParent()->getId(),'editable'=>true]);
            }

        }

        return $this->render('order/edit.html.twig', [
            'controller_name' => 'OrderController',
            'form' => $form->createView(),
            'orderItem' => $orderItem
        ]);
    }

    /**
     * @Route("/order/management", name="order_management")
     */
    public function orderMangement(Request $request, OrderRepository $orderRepository, SessionInterface $session)
    {
        $orders = $orderRepository->findBy(['isConfirmed'=>true]);

        return $this->render('order/management.html.twig', [
            'controller_name' => 'OrderController',
            'orders' => $orders
        ]);
    }

}
