<?php

namespace App\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Form\ProductType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use App\Repository\ProductRepository;

/**
 * RestProductController.
 * @Route("/api/product",name="api_")
 */
class RestProductController extends AbstractFOSRestController
{
    /**
     * @Route("/rest/rest/product", name="rest_rest_product")
     */
    public function index()
    {
        return $this->render('rest/rest_product/index.html.twig', [
            'controller_name' => 'RestProductController',
        ]);
    }

    /**
     * @Rest\Post("/create")
     *
     * @return Response
     */
    public function createProduct(Request $request)
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $data=json_decode($request->getContent(),true);

        $form->submit($data);

        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return new JsonResponse(['status'=>'created'],Response::HTTP_CREATED);
        } 

        return new JsonResponse(['status'=>'invalid data', 'data'=> $data],Response::HTTP_BAD_REQUEST);

    }

    /**
     * @Rest\Post("/update")
     *
     * @return Response
     */
    public function updateProduct(Request $request, ProductRepository $productRepository)
    {
        $data=json_decode($request->getContent(),true);

        if (array_key_exists('id', $data)) {
			$product = $productRepository->find($data['id']);

			$form = $this->createForm(ProductType::class, $product);

			$data=json_decode($request->getContent(),true);

        	$form->submit($data);

        	if ($form->isSubmitted()) {
	            $entityManager = $this->getDoctrine()->getManager();
	            $entityManager->flush();

	            return new JsonResponse(['status'=>'updated'],Response::HTTP_CREATED);
	        } 


        }

        return new JsonResponse(['status'=>'invalid data', 'data'=> $data],Response::HTTP_BAD_REQUEST);

    }

}
