<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Repository\ProductRepository;
use App\Entity\Product;

class StockManagementController extends AbstractController
{

	const ADD_ACTION = "add";
	const RETRIEVE_ACTION = "retrieve";

    /**
     * @Route("/stock/management", name="stock_management")
     */
    public function index(Request $request, ProductRepository $productRepository) : Response
    {	
    	$defaultData = ['value'=>1];
    	$form = $this->createFormBuilder($defaultData)
    				->add('product', ChoiceType::class, [
    					'choices' => $productRepository->findAll(), 
    					'choice_value' => 'id', 
    					'choice_label' => function(?Product $product) {
    						return $product ? $product->getTitle() : '';
    					}
    				])
    				->add('action', ChoiceType::class, ['choices' => ['Add' => StockManagementController::ADD_ACTION, 'Retrieve' => StockManagementController::RETRIEVE_ACTION ]])
    				->add('value', NumberType::class)
    				->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	$data = $form->getData();
            $product = $productRepository->find($data['product']);
           
            switch ($data['action']) {
            	case StockManagementController::ADD_ACTION:
            		$product->addQty($data['value']);
            		break;
            	case StockManagementController::RETRIEVE_ACTION:
            		$product->retrieveQty($data['value']);
            		break;
            	default:
            		# code...
            		break;
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Stock of '. $product->getTitle() .' updated successfuly!');

        }

        return $this->render('stock_management/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
