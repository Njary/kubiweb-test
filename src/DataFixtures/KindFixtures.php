<?php

namespace App\DataFixtures;

use App\Entity\Kind;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class KindFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($count = 0; $count < 5; $count++) {
            $kind = new Kind();
            $kind->setTitle("Kind " . $count);
            $manager->persist($kind);
        }

        $manager->flush();
    }
}
