<?php

namespace App\DataFixtures;

use App\Entity\Supplier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SupplierFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($count = 0; $count < 5; $count++) {
            $supplier = new Supplier();
            $supplier->setTitle("Supplier " . $count);
            $manager->persist($supplier);
        }

        $manager->flush();
    }
}
