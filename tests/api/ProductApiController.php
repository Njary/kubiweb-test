<?php

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpClient\HttpClient;

class ProductApiController extends TestCase
{

    public function testCreate() {

    	$client = HttpClient::create();

    	$data = array(
			  "title" => "testApi#".rand(0, 999),
			  "description" =>  "hello world",
			  "stockQty" => rand(0,50),
			  "priceTTC" => rand(100, 999),
			  "mark" => 1,
			  "type" => 1,
			  "kind" => 7
        );

    	$response = $client->request('POST', 'http://127.0.0.1:8000/api/product/create', ['json'=>$data]);

        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testUpdate() {

        $client = HttpClient::create();

        $data = array(
              "id" => 1,
              "title" => "testApi#".rand(0, 999),
              "description" =>  "hello world",
              "stockQty" => rand(0,50),
              "priceTTC" => rand(100, 999),
              "mark" => 1,
              "type" => 1,
              "kind" => 7
        );

        $response = $client->request('POST', 'http://127.0.0.1:8000/api/product/update', ['json'=>$data]);

        $this->assertEquals(201, $response->getStatusCode());
    }

}
